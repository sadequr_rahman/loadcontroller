import serial
import unittest
import time

# configure the serial connections (the parameters differs on the device you are connecting to)
ser = serial.Serial(port='COM3',baudrate=2400)
ser.close()

class SmartSwitchProtocolTest(unittest.TestCase):
    def setUp(self):
        ser.open()
        
    def tearDown(self):
        ser.close()
        
    def test_allOnTest(self):
        byteToSend = [252,1,0,1]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(0.5)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,2,0,1]
        TestFeedback = bytearray(byteToRec)
        i = 0
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "All On Test Fail")

    def test_ChannelOneOnTest(self):
        byteToSend = [252,1,1,1]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(0.5)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,2,1,1]
        TestFeedback = bytearray(byteToRec)
        i = 0
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "Channel one on Test Fail")

    def test_ChannelTwoOnTest(self):
        byteToSend = [252,1,2,1]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(0.5)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,2,2,1]
        TestFeedback = bytearray(byteToRec)
        i = 0
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "Channel Two on Test Fail")

    def test_ChannelThreeOnTest(self):
        byteToSend = [252,1,3,1]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(0.5)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,2,3,1]
        TestFeedback = bytearray(byteToRec)
        i = 0
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "Channel Three on Test Fail")

    def test_ChannelFourOnTest(self):
        byteToSend = [252,1,4,1]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(0.5)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,2,4,1]
        TestFeedback = bytearray(byteToRec)
        i = 0
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "Channel Four on Test Fail")

    def test_ChannelFiveOnTest(self):
        byteToSend = [252,1,5,1]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(0.5)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,2,5,1]
        TestFeedback = bytearray(byteToRec)
        i = 0
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "Channel Five on Test Fail")

    def test_ChannelSixOnTest(self):
        byteToSend = [252,1,6,1]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(0.5)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,2,6,1]
        TestFeedback = bytearray(byteToRec)
        i = 0
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "Channel Six on Test Fail")

    def test_AllOffTest(self):
        byteToSend = [252,1,0,0]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(0.5)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,2,0,0]
        TestFeedback = bytearray(byteToRec)
        i = 0
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "All channel Off Test Fail")

    def test_ChannelOneOffTest(self):
        byteToSend = [252,1,1,0]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(0.5)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,2,1,0]
        TestFeedback = bytearray(byteToRec)
        i = 0
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "Channel one off Test Fail")
    def test_ChannelTwoOffTest(self):
        byteToSend = [252,1,2,0]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(0.5)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,2,2,0]
        TestFeedback = bytearray(byteToRec)
        i = 0
        for i in range(0,3):
            self.assertEqual( TestFeedback[i], FeedBack[i], "Channel Two off Test Fail")

    def test_ChannelThreeOffTest(self):
        byteToSend = [252,1,3,0]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(0.5)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,2,3,0]
        TestFeedback = bytearray(byteToRec)
        i = 0
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "Channel Three off Test Fail")

    def test_ChannelFourOffTest(self):
        byteToSend = [252,1,4,0]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(0.5)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,2,4,0]
        TestFeedback = bytearray(byteToRec)
        i = 0
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "Channel Four off Test Fail")

    def test_ChannelFiveOffTest(self):
        byteToSend = [252,1,5,0]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(0.5)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,2,5,0]
        TestFeedback = bytearray(byteToRec)
        i = 0
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "Channel Five off Test Fail")

    def test_ChannelSixOffTest(self):
        byteToSend = [252,1,6,0]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(0.5)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,2,6,0]
        TestFeedback = bytearray(byteToRec)
        i = 0
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "Channel Six off Test Fail")

    def test_AllChannelDimTestWithoutDimEnabled(self):
        DimDisableCmd = [252,63,0,0]
        cmd = bytearray(DimDisableCmd)
        ser.write(cmd)
        time.sleep(0.5)
        test = ser.read(4)
        ser.flush()
        byteToSend = [252,51,0,50]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(0.5)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,52,0,50]
        TestFeedback = bytearray(byteToRec)
        i = 0
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "All Channel Dim Test without Dim Enabled Fail")

    def test_ChannelOneDimTestWithDimEnable(self):
        DimDisableCmd = [252,63,1,1]
        cmd = bytearray(DimDisableCmd)
        ser.write(cmd)
        time.sleep(0.5)
        test = ser.read(4)
        ser.flush()
        byteToSend = [252,51,1,50]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(0.5)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,52,1,50]
        TestFeedback = bytearray(byteToRec)
        i = 0
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "Channel One Dim Test with Dim Enable Fail")

    def test_ChannelOneDimTestWithoutDimEnable(self):
        DimDisableCmd = [252,63,1,0]
        cmd = bytearray(DimDisableCmd)
        ser.write(cmd)
        time.sleep(0.5)
        test = ser.read(4)
        ser.flush()
        byteToSend = [252,51,1,50]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(0.5)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,52,1,50]
        TestFeedback = bytearray(byteToRec)
        i = 0
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "Channel One Dim Test without Dim Enable Fail")

    def test_ChannelTwoDimTestWithDimEnable(self):
        DimDisableCmd = [252,63,2,1]
        cmd = bytearray(DimDisableCmd)
        ser.write(cmd)
        time.sleep(0.5)
        test = ser.read(4)
        ser.flush()
        byteToSend = [252,51,2,50]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(0.5)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,52,2,50]
        TestFeedback = bytearray(byteToRec)
        i = 0
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "Channel Two Dim Test with Dim Enable Fail")

    def test_ChannelTwoDimTestWithoutDimEnable(self):
        DimDisableCmd = [252,63,2,0]
        cmd = bytearray(DimDisableCmd)
        ser.write(cmd)
        time.sleep(0.5)
        test = ser.read(4)
        ser.flush()
        byteToSend = [252,51,2,50]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(0.5)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,52,2,50]
        TestFeedback = bytearray(byteToRec)
        i = 0
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "Channel Two Dim Test without Dim Enable Fail")

    def test_ChannelThreeDimTestWithDimEnable(self):
        DimDisableCmd = [252,63,3,1]
        cmd = bytearray(DimDisableCmd)
        ser.write(cmd)
        time.sleep(0.5)
        test = ser.read(4)
        ser.flush()
        byteToSend = [252,51,3,50]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(0.5)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,52,3,50]
        TestFeedback = bytearray(byteToRec)
        i = 0
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "Channel Three Dim Test with Dim Enable Fail")

    def test_ChannelThreeDimTestWithoutDimEnable(self):
        DimDisableCmd = [252,63,3,0]
        cmd = bytearray(DimDisableCmd)
        ser.write(cmd)
        time.sleep(0.5)
        test = ser.read(4)
        ser.flush()
        byteToSend = [252,51,3,50]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(0.5)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,52,3,50]
        TestFeedback = bytearray(byteToRec)
        i = 0
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "Channel Three Dim Test without Dim Enable Fail")

    def test_ChannelFourDimTestWithDimEnable(self):
        DimDisableCmd = [252,63,4,1]
        cmd = bytearray(DimDisableCmd)
        ser.write(cmd)
        time.sleep(0.5)
        test = ser.read(4)
        ser.flush()
        byteToSend = [252,51,4,50]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(0.5)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,52,4,50]
        TestFeedback = bytearray(byteToRec)
        i = 0
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "Channel Four Dim Test with Dim Enable Fail")

    def test_ChannelFourDimTestWithoutDimEnable(self):
        DimDisableCmd = [252,63,4,0]
        cmd = bytearray(DimDisableCmd)
        ser.write(cmd)
        time.sleep(0.5)
        test = ser.read(4)
        ser.flush()
        byteToSend = [252,51,4,50]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(0.5)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,52,4,50]
        TestFeedback = bytearray(byteToRec)
        i = 0
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "Channel Four Dim Test without Dim Enable Fail")

    def test_ChannelFiveDimTestWithDimEnable(self):
        DimDisableCmd = [252,63,5,1]
        cmd = bytearray(DimDisableCmd)
        ser.write(cmd)
        time.sleep(0.5)
        test = ser.read(4)
        ser.flush()
        byteToSend = [252,51,5,50]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(0.5)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,52,5,50]
        TestFeedback = bytearray(byteToRec)
        i = 0
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "Channel Five Dim Test with Dim Enable Fail")

    def test_ChannelFiveDimTestWithoutDimEnable(self):
        DimDisableCmd = [252,63,5,0]
        cmd = bytearray(DimDisableCmd)
        ser.write(cmd)
        time.sleep(0.5)
        test = ser.read(4)
        ser.flush()
        byteToSend = [252,51,5,50]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(0.5)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,52,5,50]
        TestFeedback = bytearray(byteToRec)
        i = 0
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "Channel Five Dim Test without Dim Enable Fail")

    def test_ChannelSixDimTestWithDimEnable(self):
        DimDisableCmd = [252,63,6,1]
        cmd = bytearray(DimDisableCmd)
        ser.write(cmd)
        time.sleep(0.5)
        test = ser.read(4)
        ser.flush()
        byteToSend = [252,51,6,50]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(0.5)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,52,6,50]
        TestFeedback = bytearray(byteToRec)
        i = 0
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "Channel Six Dim Test with Dim Enable Fail")

    def test_ChannelSixDimTestWithoutDimEnable(self):
        DimDisableCmd = [252,63,6,0]
        cmd = bytearray(DimDisableCmd)
        ser.write(cmd)
        time.sleep(0.5)
        test = ser.read(4)
        ser.flush()
        byteToSend = [252,51,6,50]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(0.5)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,52,6,50]
        TestFeedback = bytearray(byteToRec)
        i = 0
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "Channel Six Dim Test without Dim Enable Fail")

    def test_AllIndicatorOnTest(self):
        byteToSend = [252,53,0,1]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(0.5)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,54,0,1]
        TestFeedback = bytearray(byteToRec)
        i = 0
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "All Indicator On test Fail")

    def test_AllIndicatorOffTest(self):
        byteToSend = [252,53,0,0]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(0.5)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,54,0,0]
        TestFeedback = bytearray(byteToRec)
        i = 0
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "All Indicator Off test Fail")

    def test_IndicatorOneOnTest(self):
        byteToSend = [252,53,1,1]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(0.5)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,54,1,1]
        TestFeedback = bytearray(byteToRec)
        i = 0
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "Indicator one On test Fail")

    def test_IndicatorTwoOnTest(self):
        byteToSend = [252,53,2,1]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(0.5)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,54,2,1]
        TestFeedback = bytearray(byteToRec)
        i = 0
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "Indicator Two On test Fail")

    def test_IndicatorThreeOnTest(self):
        byteToSend = [252,53,3,1]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(0.5)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,54,3,1]
        TestFeedback = bytearray(byteToRec)
        i = 0
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "Indicator three On test Fail")

    def test_IndicatorFourOnTest(self):
        byteToSend = [252,53,4,1]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(0.5)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,54,4,1]
        TestFeedback = bytearray(byteToRec)
        i = 0
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "Indicator four On test Fail")

    def test_IndicatorFiveOnTest(self):
        byteToSend = [252,53,5,1]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(0.5)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,54,5,1]
        TestFeedback = bytearray(byteToRec)
        i = 0
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "Indicator Five On test Fail")

    def test_IndicatorSixOnTest(self):
        byteToSend = [252,53,6,1]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(0.5)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,54,6,1]
        TestFeedback = bytearray(byteToRec)
        i = 0
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "Indicator Six On test Fail")

    def test_IndicatorOneOffTest(self):
        byteToSend = [252,53,1,0]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(0.5)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,54,1,0]
        TestFeedback = bytearray(byteToRec)
        i = 0
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "Indicator one Off test Fail")

    def test_IndicatorTwoOffTest(self):
        byteToSend = [252,53,2,0]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(0.5)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,54,2,0]
        TestFeedback = bytearray(byteToRec)
        i = 0
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "Indicator Two Off test Fail")

    def test_IndicatorThreeOffTest(self):
        byteToSend = [252,53,3,0]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(0.5)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,54,3,0]
        TestFeedback = bytearray(byteToRec)
        i = 0
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "Indicator three Off test Fail")

    def test_IndicatorFourOffTest(self):
        byteToSend = [252,53,4,0]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(0.5)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,54,4,0]
        TestFeedback = bytearray(byteToRec)
        i = 0
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "Indicator four Off test Fail")

    def test_IndicatorFiveOffTest(self):
        byteToSend = [252,53,5,0]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(0.5)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,54,5,0]
        TestFeedback = bytearray(byteToRec)
        i = 0
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "Indicator Five Off test Fail")

    def test_IndicatorSixOffTest(self):
        byteToSend = [252,53,6,0]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(0.5)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,54,6,0]
        TestFeedback = bytearray(byteToRec)
        i = 0
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "Indicator Six Off test Fail")

    def test_ALLIndicatorBlinkTest(self):
        byteToSend = [252,67,0,10]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(1)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,68,0,10]
        TestFeedback = bytearray(byteToRec)
        i = 0
        time.sleep(2.0)
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "All Indicator Blink test Fail")

    def test_IndicatorOneBlinkTest(self):
        byteToSend = [252,67,1,5]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(1)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,68,1,5]
        TestFeedback = bytearray(byteToRec)
        i = 0
        time.sleep(2.0)
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "Indicator One Blink test Fail")

    def test_IndicatorTwoBlinkTest(self):
        byteToSend = [252,67,2,5]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(1)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,68,2,5]
        TestFeedback = bytearray(byteToRec)
        i = 0
        time.sleep(2.0)
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "Indicator Two Blink test Fail")

    def test_IndicatorThreeBlinkTest(self):
        byteToSend = [252,67,3,5]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(1)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,68,3,5]
        TestFeedback = bytearray(byteToRec)
        i = 0
        time.sleep(2.0)
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "Indicator Three Blink test Fail")

    def test_IndicatorFourBlinkTest(self):
        byteToSend = [252,67,4,5]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(1)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,68,4,5]
        TestFeedback = bytearray(byteToRec)
        i = 0
        time.sleep(2.0)
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "Indicator Four Blink test Fail")

    def test_IndicatorFiveBlinkTest(self):
        byteToSend = [252,67,5,5]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(1)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,68,5,5]
        TestFeedback = bytearray(byteToRec)
        i = 0
        time.sleep(2.0)
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "Indicator Five Blink test Fail")

    def test_IndicatorSixBlinkTest(self):
        byteToSend = [252,67,6,5]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(1)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,68,6,5]
        TestFeedback = bytearray(byteToRec)
        i = 0
        time.sleep(2.0)
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "Indicator Six Blink test Fail")

    def test_IndicatorSequence0Test(self):
        byteToSend = [252,69,0,0]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(1)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,70,0,0]
        TestFeedback = bytearray(byteToRec)
        i = 0
        time.sleep(5.0)
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "Indicator Sequence0 test Fail")

    def test_IndicatorSequence1Test(self):
        byteToSend = [252,69,0,1]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(0.5)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,70,0,1]
        TestFeedback = bytearray(byteToRec)
        i = 0
        time.sleep(5.0)
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "Indicator Sequence1 test Fail")

    def test_IndicatorSequence2Test(self):
        byteToSend = [252,69,0,2]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(0.5)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,70,0,2]
        TestFeedback = bytearray(byteToRec)
        i = 0
        time.sleep(5.0)
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "Indicator Sequence2 test Fail")

    def test_IndicatorSequenceAlterTest(self):
        byteToSend = [252,69,0,3]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(0.5)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,70,0,3]
        TestFeedback = bytearray(byteToRec)
        i = 0
        time.sleep(5.0)
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "Indicator Sequence Alter test Fail")

    def test_IndicatorColorMoode0Test(self):
        byteToSend = [252,11,0,0]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(0.5)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,12,0,0]
        TestFeedback = bytearray(byteToRec)
        i = 0
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "Indicator Color Mood0 test Fail")

    def test_IndicatorColorMoode1Test(self):
        byteToSend = [252,11,0,1]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(0.5)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,12,0,1]
        TestFeedback = bytearray(byteToRec)
        i = 0
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "Indicator Color Mood1 test Fail")

    def test_AllChannelDimEnableTest(self):
        byteToSend = [252,63,0,1]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(0.5)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,64,0,1]
        TestFeedback = bytearray(byteToRec)
        i = 0
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "All Channel Dim Enable test Fail")

    def test_AllChannelDimDisableTest(self):
        byteToSend = [252,63,0,0]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(0.5)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,64,0,0]
        TestFeedback = bytearray(byteToRec)
        i = 0
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "All Channel Dim Disable test Fail")

    def test_ChannelOneDimEnableTest(self):
        byteToSend = [252,63,1,1]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(0.5)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,64,1,1]
        TestFeedback = bytearray(byteToRec)
        i = 0
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "Channel 1 Dim enable test Fail")

    def test_ChannelTwoDimEnableTest(self):
        byteToSend = [252,63,2,1]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(0.5)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,64,2,1]
        TestFeedback = bytearray(byteToRec)
        i = 0
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "Channel 2 Dim enable test Fail")

    def test_ChannelThreeDimEnableTest(self):
        byteToSend = [252,63,3,1]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(0.5)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,64,3,1]
        TestFeedback = bytearray(byteToRec)
        i = 0
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "Channel 3 Dim enable test Fail")

    def test_ChannelFourDimEnableTest(self):
        byteToSend = [252,63,4,1]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(0.5)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,64,4,1]
        TestFeedback = bytearray(byteToRec)
        i = 0
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "Channel 4 Dim enable test Fail")

    def test_ChannelFiveDimEnableTest(self):
        byteToSend = [252,63,5,1]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(0.5)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,64,5,1]
        TestFeedback = bytearray(byteToRec)
        i = 0
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "Channel 5 Dim enable test Fail")

    def test_ChannelSixDimEnableTest(self):
        byteToSend = [252,63,6,1]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(0.5)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,64,6,1]
        TestFeedback = bytearray(byteToRec)
        i = 0
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "Channel 6 Dim enable test Fail")

    def test_ChannelOneDimDisableTest(self):
        byteToSend = [252,63,1,0]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(0.5)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,64,1,0]
        TestFeedback = bytearray(byteToRec)
        i = 0
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "Channel 1 Dim disable test Fail")

    def test_ChannelTwoDimDisableTest(self):
        byteToSend = [252,63,2,0]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(0.5)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,64,2,0]
        TestFeedback = bytearray(byteToRec)
        i = 0
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "Channel 2 Dim disable test Fail")

    def test_ChannelThreeDimDisableTest(self):
        byteToSend = [252,63,3,0]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(0.5)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,64,3,0]
        TestFeedback = bytearray(byteToRec)
        i = 0
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "Channel 3 Dim disable test Fail")

    def test_ChannelFourDimDisableTest(self):
        byteToSend = [252,63,4,0]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(0.5)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,64,4,0]
        TestFeedback = bytearray(byteToRec)
        i = 0
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "Channel 4 Dim disable test Fail")

    def test_ChannelFiveDimDisableTest(self):
        byteToSend = [252,63,5,0]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(0.5)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,64,5,0]
        TestFeedback = bytearray(byteToRec)
        i = 0
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "Channel 5 Dim disable test Fail")

    def test_ChannelSixDimDisableTest(self):
        byteToSend = [252,63,6,0]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(0.5)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,64,6,0]
        TestFeedback = bytearray(byteToRec)
        i = 0
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "Channel 6 Dim disable test Fail")

    def test_AllChannelHardDimEnableTest(self):
        byteToSend = [252,63,0,2]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(0.5)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,64,0,2]
        TestFeedback = bytearray(byteToRec)
        i = 0
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "All Channel Hardware Dim Disable test Fail")

    def test_ChannelOneHardwareDimEnableTest(self):
        byteToSend = [252,63,1,2]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(0.5)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,64,1,2]
        TestFeedback = bytearray(byteToRec)
        i = 0
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "Channel 1 HardwareDim enable test Fail")

    def test_ChannelTwoHardwareDimEnableTest(self):
        byteToSend = [252,63,2,2]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(0.5)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,64,2,2]
        TestFeedback = bytearray(byteToRec)
        i = 0
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "Channel 2 HardwareDim enable test Fail")

    def test_ChannelThreeHardwareDimEnableTest(self):
        byteToSend = [252,63,3,2]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(0.5)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,64,3,2]
        TestFeedback = bytearray(byteToRec)
        i = 0
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "Channel 3 Dim enable test Fail")

    def test_ChannelFourHardwareDimEnableTest(self):
        byteToSend = [252,63,4,2]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(0.5)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,64,4,2]
        TestFeedback = bytearray(byteToRec)
        i = 0
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "Channel 4 HardwareDim enable test Fail")

    def test_ChannelFiveHardwareDimEnableTest(self):
        byteToSend = [252,63,5,2]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(0.5)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,64,5,2]
        TestFeedback = bytearray(byteToRec)
        i = 0
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "Channel 5 HardwareDim enable test Fail")

    def test_ChannelSixHardwareDimEnableTest(self):
        byteToSend = [252,63,6,2]
        stimuli = bytearray(byteToSend)
        ser.write(stimuli)
        time.sleep(0.5)
        rec = ser.read(4)
        FeedBack = bytearray(rec)
        byteToRec = [253,64,6,2]
        TestFeedback = bytearray(byteToRec)
        i = 0
        for i in range(0,4):
            self.assertEqual( TestFeedback[i], FeedBack[i], "Channel 6 HardwareDim enable test Fail")


if __name__ == '__main__':
    unittest.main()
