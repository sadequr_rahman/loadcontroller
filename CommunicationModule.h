/*
 * CommunicationModule.h
 *
 * Created: 11/16/2015 12:14:57 PM
 *  Author: Aplomb_Tech
 */ 


#ifndef COMMUNICATIONMODULE_H_
#define COMMUNICATIONMODULE_H_

#include "CommonHeader.h"


#define StatPacket		0xFC
#define FB_APP			0xFD
#define FB_MAN			0XFE
#define REC_BYTE_NUM	0x03


#define PRESS_EVENT_ON_OFF_FB		201
#define PRESS_EVENT_DIMMING_FB		202
#define PRESS_EVENT_COMBO_FB		203



void ComModuleInit(unsigned int);
void SendFeedBack(void);
void Transmit(uint8_t);
/*void feedBackUpdate(DataModel_t *);*/

#endif /* COMMUNICATIONMODULE_H_ */