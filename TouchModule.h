/*
 * TouchModule.h
 *
 * Created: 11/13/2015 12:37:11 PM
 *  Author: Aplomb_Tech
 */ 


#ifndef TOUCHMODULE_H_
#define TOUCHMODULE_H_

#include "CommonHeader.h"

#define eKT5211_Read_Address 0X61
#define eKT5211_Write_Address 0X60

#define TOUCH_REST				PORTB3
#define TOUCH_RESET_PORT		PORTB
#define TOUCH_RESET_DDR			DDRB


#define Button1		0x08
#define Button2		0x10
#define Button3		0x20
#define Button4		0x01
#define Button5		0x04
#define Button6		0x02
#define Button1_6	0x0A
#define Button2_5	0x14



void TouchModuleinit(void);
void twi_start(void);
void twi_add(unsigned int);
void twi_write(unsigned int);
unsigned int twi_read(void);
void twi_stop(void);

void writeReg(unsigned int,unsigned int);
unsigned int readReg(unsigned int);

unsigned int getButtonStatus1(void);
void MultiButtonConfig(uint8_t);
void TouchHardReset(void);


#endif /* I2C_H_ */