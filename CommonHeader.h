/*
 * CommonHeader.h
 *
 * Created: 11/12/2015 2:46:13 PM
 *  Author: Aplomb_Tech
 */ 


#ifndef COMMONHEADER_H_
#define COMMONHEADER_H_

#define F_CPU 8000000UL

#define BAUD 2400
#define MYUBRR F_CPU/16/BAUD-1

#define DEBOUNCE		50
#define True			1
#define False			0

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#define setBit(x,y)		(x |= (1<<y))
#define clrBit(x,y)		(x &= ~(1<<y))
#define toggleBit(x,y)  (x ^= (1<<y))
#define ChkBit(x,y)		(x & (1<<y))

typedef struct 
{
	volatile uint8_t Command;
	volatile uint8_t ChID;
	volatile uint8_t _value;
	volatile uint8_t Flag;
}DataModel_t;

typedef struct {
	volatile uint8_t LoadStatus;
	volatile uint8_t IsDimmingEnabled;
	volatile uint8_t DimValue;
	volatile uint8_t DimMaxValue;
	volatile uint8_t DimMinValue;
}Load_t;

Load_t LoadAttributes[6];
DataModel_t RxData,TxData;

typedef enum {
	LOAD_ONOFF_CRTL			= 1,
	LOAD_DIM_CRTL			= 51,
	LOAD_TH_SHUTDOWN_CRTL	= 55,
	INDICATOR_ONOFF_CTRL	= 53,
	INDICATOR_BLINK_CTRL	= 67,
	INDICATOR_SEQUENCE_CTRL	= 69,
	INDICATOR_COLOR_CTRL	= 11,
	CNANNEL_MOOD_CONFIG		= 63,
	CHANNEL_MOOD_SET_DIM_MAX= 59,
	CHANNEL_MOOD_SET_DIM_MIN= 65,
	STATUS_LOAD_ONOFF		= 5,
	STATUS_LOAD_DIM_VALUE	= 27,
}Cmd_t;
		
typedef enum
{
	Normal_Mood,
	Single_Blink_Mood,
	ALL_Blink_Mood,
	SequenceU_Mood,
	SequenceD_Mood,
}Indicator_Event_t;

#endif /* COMMONHEADER_H_ */