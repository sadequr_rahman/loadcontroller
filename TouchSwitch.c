 /*
  * TouchSwitch.c
  *
  * Created: 11/12/2015 10:48:04 AM
  *  Author: Aplomb_Tech
  */ 
 
#include "CommonHeader.h"
#include "IndicatorModule.h"
#include "TouchModule.h"
#include "SysTime.h"
#include "CommunicationModule.h"
#include "LoadControlModule.h"
#include "EepromModule.h"

#include <avr/wdt.h>

 
 
static const uint8_t BUTTON[6] = {Button6,Button5,Button4,Button3,Button2,Button1};
static const Indicator_Event_t temp[3] = {Normal_Mood,SequenceU_Mood,SequenceD_Mood};
	  
 int main(void)
 { 	
	
	
	IndicatorModuleInit();
	clrBit(CTLPORT,LED_CTL);
	
	if(ChkBit(WDTCSR,WDIF))
	{
		clrBit(WDTCSR,WDIF);
	}
	wdt_enable(WDTO_2S); 
	
	Indicator_Event_t event = Normal_Mood;
 	extern uint8_t blink_f[6];
 	uint8_t Blink_Count = 0;
 	uint8_t UserBlinkNumber = 4;
 	uint8_t LedID = 0;
 	uint16_t swT=0;
 	uint16_t time = 0;
 	uint8_t _button = 0;
 	uint8_t control = 0;
 	uint8_t _pressCount[6] = {0,0,0,0,0,0};
 	uint8_t _ThShuntDown = 0;
 	uint8_t SequenceAlter_F = 0;
 	uint8_t _pressCoutC = 0;
 	uint8_t comboEvent_F = 1;
	uint8_t i = 0;
	uint8_t sendFlag = 0;
	uint16_t SendTime = 0;
	uint8_t UpDown_F = 0;
	uint8_t releaseF = 0; 
 	 
	  
 	ComModuleInit(MYUBRR);
 	LoadModuleInit();
 	Timer1Init();
 	TouchModuleinit();
	TouchHardReset();
	wdt_reset();
 	_delay_ms(1200);
 	MultiButtonConfig(True);
	restore();
 	for (i=0; i<6;i++)
 	{
 		if (LoadAttributes[i].LoadStatus > True )
 		{
 			LoadAttributes[i].LoadStatus = False;
 		}
 		if (LoadAttributes[i].IsDimmingEnabled > True)
 		{
 			LoadAttributes[i].IsDimmingEnabled = False;
 		}
 		if (LoadAttributes[i].DimValue > HIGH_LIMIT)
 		{
 			LoadAttributes[i].DimValue = LOW_LIMIT;
 		}
 		if (LoadAttributes[i].DimMaxValue > HIGH_LIMIT)
 		{
 			LoadAttributes[i].DimMaxValue = HIGH_LIMIT;
 		}
 		if (LoadAttributes[i].DimMinValue > HIGH_LIMIT)
 		{
 			LoadAttributes[i].DimMinValue = LOW_LIMIT;
 		}
 	}
	 
	 wdt_reset();
	 setBit(CTLPORT,LED_CTL);
	 
 	while (True)
 	{
 		
		 /* Indicator control Block  */
		if (event == Normal_Mood)
 		{
			 IndicatorUpdateStatus();
 		}
		else
		{
			if (getmilis() - time >= 200 )
			{
				time = getmilis();
				
				if (event == Single_Blink_Mood)
				{
					blink_f[LedID] = True;
					Blink_Count++;
					if (Blink_Count >= UserBlinkNumber)
					{
						event = Normal_Mood;
						Blink_Count = False;
					}
				}
				else if (event == ALL_Blink_Mood)
				{
					for (uint8_t i = 0; i < 6 ;i++)
					{
						blink_f[i] = True;
						if (Blink_Count == 0)
						{
							IndicatorOnOff(i+1,False);
						}
					}
					Blink_Count++;
					if (Blink_Count >= UserBlinkNumber)
					{
						event = Normal_Mood;
						Blink_Count = False;
					}
				}
				else if (event == SequenceU_Mood || event == SequenceD_Mood)
				{
					if (event == SequenceD_Mood)
					{
						blink_f[control] = True;
					}
					else
					{
						blink_f[5 - control] = True;
					}
					if (control==0)
					{
						for (i = 1; i<=6;i++)
						{
							IndicatorOnOff(i,False);
						}
					}
					control++;
					if (control >= 7)
					{
						control = 0;
						if (SequenceAlter_F == True)
						{
							(event==SequenceD_Mood)?(event=SequenceU_Mood):(event=SequenceD_Mood);
							
						}
						Blink_Count++;
					}
					if (Blink_Count >= 6)
					{
						event = Normal_Mood;
						Blink_Count = False;
						SequenceAlter_F = False;
					}
					
				}
				
			}
		}

/* Thermal ShutDown check */
if (_ThShuntDown == False)
{
 	 if (sendFlag == True)
 	 {
		  if (getmilis() - SendTime >= 200)
		  {
			  SendTime = getmilis();
			  sendFlag = False;
			  SendFeedBack();
		  }
 	 }
	 
	 /* Touch Controller Block for Sense Press Event */
 	if (getmilis() - swT >= DEBOUNCE)
 	{
 		swT= getmilis();
 		_button = getButtonStatus1();
		TxData.Flag = FB_MAN;
 		for (i = 0; i < 6 ; i++)
 		{
			if (_button == BUTTON[i])
			{
				if (releaseF == 0)
				{
					_pressCount[i]++;
					if (_pressCount[i] >= 20 && LoadAttributes[i].IsDimmingEnabled==2)
					{
						if (UpDown_F == 1)
						{
							LoadAttributes[i].DimValue++;
							sendFlag = True;
							TxData.ChID = i+1;
							TxData.Command = PRESS_EVENT_DIMMING_FB;
							TxData._value  = LoadAttributes[i].DimValue;
							if (LoadAttributes[i].DimValue >= HIGH_LIMIT)
							{
								LoadAttributes[i].DimValue = HIGH_LIMIT;
								UpDown_F = 0;
								releaseF = 1;
							}
						}
						if (UpDown_F == 0)
						{
							LoadAttributes[i].DimValue--;
							sendFlag = True;
							TxData.ChID = i+1;
							TxData.Command = PRESS_EVENT_DIMMING_FB;
							TxData._value  = LoadAttributes[i].DimValue;
							if (LoadAttributes[i].DimValue <= LOW_LIMIT)
							{
								LoadAttributes[i].DimValue = LOW_LIMIT;
								UpDown_F = 1;
								releaseF = 1;
							}
						}
	
					}
				}
			}
 		}
		
		if(_button != False  && comboEvent_F == True)
		{
			_pressCoutC++;
			if (_pressCoutC >= 20)
			{
				TxData.Command = PRESS_EVENT_COMBO_FB;
				TxData._value  = _button;
				sendFlag = True;
				_pressCoutC = 0;
				comboEvent_F = False;
			}
		}
 	}
 
 	if (_button == False)
 	{
 	  releaseF = 0;
	 comboEvent_F = True;
	 _pressCoutC = 0;
	for (i = 0 ; i < 6 ; i++)
 		{
			if (_pressCount[i] > 0 && _pressCount[i] < 10)
 			{
 				LoadAttributes[i].LoadStatus ? (LoadAttributes[i].LoadStatus = False):(LoadAttributes[i].LoadStatus = True);
 				  TxData.Command = PRESS_EVENT_ON_OFF_FB;
				  TxData.ChID	 = i+1;
				  TxData._value  = LoadAttributes[i].LoadStatus;
 				  sendFlag = True;
 			}
				_pressCount[i] = False;
 		}			
 	}
 }
 	
 /* Communication Module Data Process block for incoming data.	*/
 		if (RxData.Flag == True)
 		{
 			RxData.Flag = False ;
			Cmd_t  Command	= RxData.Command;
			TxData.Flag		= FB_APP;
			TxData.ChID		= RxData.ChID;
			TxData._value	= RxData._value;
			TxData.Command	= (Command + 1);
			
			switch (Command)
			{
			case LOAD_ONOFF_CRTL:
						LoadOnOFF(RxData.ChID,RxData._value);
				 break;
			case LOAD_DIM_CRTL:
						 LoadSetDimValue(RxData.ChID,(100 - RxData._value));
						 LoadOnOFF(RxData.ChID,True);
				 break;
			case LOAD_TH_SHUTDOWN_CRTL:
						if (RxData._value <=1)
						{
							_ThShuntDown = RxData._value; 
						}
				 break;
			case INDICATOR_ONOFF_CTRL:
						IndicatorOnOff(RxData.ChID,RxData._value);
				 break;
			case INDICATOR_BLINK_CTRL:
						if (RxData.ChID == 0)
						{
							event = ALL_Blink_Mood;
						}
						else
						{
							event = Single_Blink_Mood;
							LedID = RxData.ChID - 1;
						}
						UserBlinkNumber = RxData._value;
				 break;
			case INDICATOR_SEQUENCE_CTRL:
						Blink_Count = 0;
						control = 0;
						if(RxData._value == 3)
						{
							SequenceAlter_F = True;
							event = SequenceU_Mood;
						}
						else
						{
							/// need to check the rec value to match the array boundary
							if (RxData._value < 3)
							{
								event = temp[RxData._value];
								SequenceAlter_F = False;
							}	
						}
				 break;
			case INDICATOR_COLOR_CTRL:
						IndicatorColorMoodConfig(RxData._value);
				 break;
			case CNANNEL_MOOD_CONFIG:
						LoadChannelConfig(RxData.ChID,RxData._value);
				 break;
			case CHANNEL_MOOD_SET_DIM_MAX:
						LoadSetMaxDimValue(RxData.ChID,RxData._value);
				 break;
			case CHANNEL_MOOD_SET_DIM_MIN:
						LoadSetMinDimValue(RxData.ChID,RxData._value);
				 break;
			case STATUS_LOAD_ONOFF:
						if (RxData.ChID != 0)
						{
								TxData.ChID = RxData.ChID;
								TxData.Command = Command;
								TxData._value  = LoadAttributes[RxData.ChID-1].LoadStatus;		
						}
				 break;
			case STATUS_LOAD_DIM_VALUE:
						if (RxData.ChID != 0)
						{
							TxData.ChID = RxData.ChID;
							TxData.Command = Command;
							TxData._value  = LoadAttributes[RxData.ChID-1].DimValue;
						}
				 break;
			}
		SendFeedBack();	
 		}
 	save();	
 	wdt_reset();
 	}
 	
}