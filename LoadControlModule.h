/*
 * LoadControlModule.h
 *
 * Created: 11/16/2015 9:58:55 AM
 *  Author: Aplomb_Tech
 */ 


#ifndef LOADCONTROLMODULE_H_
#define LOADCONTROLMODULE_H_


#include "CommonHeader.h"

#define OUT6		PORTB0
#define OUT5		PORTD6
#define OUT4		PORTD7
#define OUT3		PORTD4
#define OUT2		PORTD5
#define OUT1		PORTD2
#define OUTDDR		DDRD
#define OUTPORT		PORTD
#define OUTDDR1		DDRB
#define OUTPORT1	PORTB
// #define OUTPUTPIN   PIND
// #define OUTPUTPIN1  PIND


#define HIGH_LIMIT	85
#define LOW_LIMIT	10
/*
LoadModuleInit(void);

This function must be called in
initialization section to enable the
Load Control Module.
*/
void LoadModuleInit(void);
/*
LoadOnOFF(ChID,Value);

This function turn on/off load
To the corresponding channel("ChId")

value -> 0 means turn off 
value -> 1 means turn on 
 
*/
void LoadOnOFF(uint8_t,uint8_t);
/*
uint8_t LoadGetStatus(ChID);

This Function returns the state of
the corresponding Channel(ChID).
*/
 //uint8_t LoadGetStatus(uint8_t);
 /*
 LoadChannelConfig(ChID,PropertyValue);
 
 This Function Configure a channel as 
 dim able or not depending on param
 "PropertyValue"
 	PropertyValue -> 0 means not Dim able
 	PropertyValue -> 1 means Dim able from apps.
	PropertyValue -> 2 means Dim able both from apps & Hardware. 
 and "ChID" refers to the corresponding Channel.
 */
void LoadChannelConfig(uint8_t , uint8_t);
 /*
 LoadSetDimValue(ChID,Value);
 
 This function set dimming value
 To the corresponding channel("ChId").
 "Value" is the DimValue.
  
 */
 void LoadSetDimValue(uint8_t,uint8_t);
// /*
// uint8_t LoadGetDimValue(ChID);
// 
// This Function return the dim value
// corresponding to the Channel ID.
// */
// uint8_t LoadGetDimValue(uint8_t);
 /*
 LoadSetMaxDimValue(ChID,value);
 
 This Function set the Max Dimming
 Limit For the corresponding Channel(ChID).
 */
 void LoadSetMaxDimValue(uint8_t,uint8_t);
// /*
// LoadGetMaxDimValue(ChID,value);
// 
// This Function returns the Max Dimming
// Limit For the corresponding Channel(ChID).
// */
// void LoadSetMaxDimValue(uint8_t,uint8_t);
// /*
// LoadSetMaxDimValue(ChID,value);
// 
// This Function set the Max Dimming
// Limit For the corresponding Channel(ChID).
// */
// void LoadSetMaxDimValue(uint8_t,uint8_t);
// /*
// LoadGetMaxDimValue(ChID);
// 
// This Function returns the Max Dimming
// Limit For the corresponding Channel(ChID).
// */
// uint8_t LoadGetMaxDimValue(uint8_t);
 /*
 LoadSetMinDimValue(ChID,value);
 
 This Function set the Min Dimming
 Limit For the corresponding Channel(ChID).
 */
 void LoadSetMinDimValue(uint8_t,uint8_t);
// /*
// LoadGetMinDimValue(ChID);
// 
// This Function returns the Min Dimming
// Limit For the corresponding Channel(ChID).
// */
// uint8_t LoadGetMinDimValue(uint8_t);

#endif /* LOADCONTROLMODULE_H_ */