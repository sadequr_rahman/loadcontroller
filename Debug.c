/*
 * Debug.c
 *
 * Created: 11/13/2015 3:24:49 PM
 *  Author: Aplomb_Tech
 */

 
#include "Debug.h"

void DebugInit(unsigned int ubrr)
{
	/*Set baud rate */
	UBRR0H |= (unsigned char)(ubrr>>8);
	UBRR0L |= (unsigned char)ubrr;
	/*Enable transmitter */
	UCSR0B |= (1<<TXEN0);
	/* Set frame format: 8data, 1stop bit */
	UCSR0C |= (3<<UCSZ00);
}

void DebugCHAR(char _data)
{
	/* Wait for empty transmit buffer */
	while ( !( UCSR0A & (1<<UDRE0)) );
	/* Put data into buffer, sends the data */
	UDR0 = _data;
}

void DebugSTR(const char *str)
{
	while (*str!='\0')
	{
		DebugCHAR(*str);
		str++;
	}
}

void DebugUINT(uint16_t _data)
{
	char str[12];
	itoa(_data,str,10);
	DebugSTR(str);
}