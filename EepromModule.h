/*
 * EepromModule.h
 *
 * Created: 11/20/2015 10:17:13 AM
 *  Author: APL-
 */ 


#ifndef EEPROMMODULE_H_
#define EEPROMMODULE_H_

#include "CommonHeader.h"
#include <avr/eeprom.h>

void save(void);
void restore(void);

#endif /* EEPROMMODULE_H_ */