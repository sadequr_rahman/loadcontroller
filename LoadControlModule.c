/*
 * LoadControlModule.c
 *
 * Created: 11/16/2015 11:15:19 AM
 *  Author: Aplomb_Tech
 */ 

#include "LoadControlModule.h"

static void OnOff(uint8_t,uint8_t);

volatile uint8_t count_zc = 0;
volatile uint8_t done[6];

void LoadModuleInit()
{
	OUTDDR   |= (1<<OUT1)|(1<<OUT2)|(1<<OUT3)|(1<<OUT4)|(1<<OUT5);
	OUTDDR1  |= (1<<OUT6);
	/* External Interrupt Enable */
	EICRA |= (1<<ISC10)|(1<<ISC11);
	EIMSK |= (1<<INT1);
	/* Timer0 Preload = 99; Actual Interrupt Time = 100 us */
	TCCR0A |= (1<<WGM01);
	OCR0A = 99;
	/* Enable Global Interrupt */
	sei();
}
static void OnOff(uint8_t _chId,uint8_t _val)
{
	switch (_chId)
	{
		case 0:
		_val?setBit(OUTPORT,OUT1):clrBit(OUTPORT,OUT1);
		break;
		case 1:
		_val?setBit(OUTPORT,OUT2):clrBit(OUTPORT,OUT2);
		break;
		case 2:
		_val?setBit(OUTPORT,OUT3):clrBit(OUTPORT,OUT3);
		break;
		case 3:
		_val?setBit(OUTPORT,OUT4):clrBit(OUTPORT,OUT4);
		break;
		case 4:
		_val?setBit(OUTPORT,OUT5):clrBit(OUTPORT,OUT5);
		break;
		case 5:
		_val?setBit(OUTPORT1,OUT6):clrBit(OUTPORT1,OUT6);
		break;
	}
}
void LoadOnOFF(uint8_t CHID,uint8_t Val)
{
	if (CHID == 0)
	{
		for (uint8_t i = 0; i<6; i++)
		{
			LoadAttributes[i].LoadStatus = Val;
		}
	}
	else
		{
			LoadAttributes[CHID-1].LoadStatus = Val;
		}
}
void LoadChannelConfig(uint8_t _chID,uint8_t _value)
{
	
	if (_chID == 0)
	{
		for (uint8_t i = 0; i<6; i++)
		{
			LoadAttributes[i].IsDimmingEnabled = _value;
		}
	}else
	{
		LoadAttributes[_chID-1].IsDimmingEnabled = _value;
	}
}
void LoadSetMaxDimValue(uint8_t _ChID,uint8_t _value)
{
	if (_ChID == 0)
	{
		for (uint8_t i = 0 ; i < 6; i++)
		{
			LoadAttributes[i].DimMaxValue= _value;
		}
	}
	else
	{
		LoadAttributes[_ChID-1].DimMaxValue= _value;
	}
	
}
void LoadSetMinDimValue(uint8_t _ChID,uint8_t _value)
{
	if (_ChID == 0)
	{
		for (uint8_t i = 0 ; i < 6; i++)
		{
			LoadAttributes[i].DimMinValue = _value;
		}
	}
	else
	{
		LoadAttributes[_ChID-1].DimMinValue = _value;
	}
	
}
void LoadSetDimValue(uint8_t _ChID,uint8_t _val)
{
	if (_ChID == 0)
	{
		for (uint8_t i = 0 ; i < 6; i++)
		{
			if ( _val >= LoadAttributes[i].DimMinValue && _val <= LoadAttributes[i].DimMaxValue)
			{
				LoadAttributes[i].DimValue = _val;
			}
			
		}
	}
	else
	{
		if ( _val >= LoadAttributes[_ChID-1].DimMinValue && _val <= LoadAttributes[_ChID-1].DimMaxValue)
		{
			LoadAttributes[_ChID-1].DimValue = _val;
		}
	}
}


/*
uint8_t LoadGetStatus(uint8_t CHID)
{
	uint8_t temp = 0;
	if (CHID == 0)
	{
		for (uint8_t i = 0; i<6; i++)
		{
			temp |= (LoadAttributes[i].LoadStatus<<i);
		}
	}
	else
	{
		temp = LoadAttributes[CHID-1].LoadStatus;
	}
	
	return temp;
}
uint8_t LoadGetMaxDimValue(uint8_t _ChID)
{
	uint8_t temp =0;
	if (_ChID == 0)
	{
		temp = 255;
	}
	else
	{
		temp = LoadAttributes[_ChID-1].DimMaxValue;
	}
	return temp;
}
uint8_t LoadGetMinDimValue(uint8_t _ChID)
{
	uint8_t temp =0;
	if (_ChID == 0)
	{
		temp = 255;
	}
	else
	{
		temp = LoadAttributes[_ChID-1].DimMinValue;
	}
	return temp;
}

uint8_t LoadGetDimValue(uint8_t _ChID)
{
	uint8_t temp =0;
	if (_ChID == 0)
	{
		temp = 255;
	}
	else
	{
		temp = LoadAttributes[_ChID-1].DimValue;
	}
	return temp;
}

*/

ISR(INT1_vect)
{
	cli();
	TCCR0B &= ~(1<<CS01);
	count_zc = 0;
	for(uint8_t i=0;i<6;i++)
	{
		done[i]=False;
		if(LoadAttributes[i].IsDimmingEnabled <= False)
		{
			done[i]=True;
			OnOff(i,LoadAttributes[i].LoadStatus);
		}
		else
		{
			 if(LoadAttributes[i].DimValue <= LOW_LIMIT)
			 {
				 OnOff(i,LoadAttributes[i].LoadStatus);
				 done[i] = True;
			 }
			 else if(LoadAttributes[i].DimValue >= HIGH_LIMIT)
			 {
				 OnOff(i,False);
				 done[i] = True;
			 }
		 
		}
		
	}
	
	TCNT0 = 0;
	TIMSK0 |= (1<<OCIE0A);
	TCCR0B |= (1<<CS01);
	sei();
}

ISR(TIMER0_COMPA_vect)
{
	count_zc++;	
	for (uint8_t i = 0; i < 6 ; i++)
	{
			if((count_zc>LoadAttributes[i].DimValue) && (done[i] == False))
				{
					OnOff(i,LoadAttributes[i].LoadStatus);
					for (int k = 0; k < 100; k++)
					{
						__asm__ __volatile__ ("nop");
					}
					OnOff(i,False);
					done[i] = True;
				}
	}
	// Stop Timer	
	if (count_zc >= HIGH_LIMIT)
	{
		TCCR0B &= ~(1<<CS01);
		count_zc = 0;
		TCNT0 = 0;
	}
}