/*
 * EepromModule.c
 *
 * Created: 11/20/2015 10:42:56 AM
 *  Author: APL-
 */ 

#include "EepromModule.h"

Load_t EEMEM EELoad[6];


void save(void){
	// Create a memory block with the same structure as the eeprom
	Load_t temp[6];
	// Gather all variables to be saved into the structure
	for (uint8_t i = 0; i < 6 ; i++)
	{
		temp[i].LoadStatus			= LoadAttributes[i].LoadStatus;
		temp[i].IsDimmingEnabled	= LoadAttributes[i].IsDimmingEnabled;
		temp[i].DimValue			= LoadAttributes[i].DimValue;
		temp[i].DimMaxValue			= LoadAttributes[i].DimMaxValue;
		temp[i].DimMinValue			= LoadAttributes[i].DimMinValue;
	// Now write the block out to eeprom
	eeprom_update_block(&temp[i], &EELoad[i], 5);
	}
}

void restore(void){
	// Create a memory block with the same structure as the eeprom
	Load_t temp[6];
	
	for (uint8_t i= 0; i < 6 ; i++)
	{
		// Read the data from eeprom into the 'temp' version in memory
		eeprom_read_block( &temp[i], &EELoad[i], 5);
		// Now copy the variables out into their proper slots
		LoadAttributes[i].LoadStatus = temp[i].LoadStatus;
		LoadAttributes[i].IsDimmingEnabled = temp[i].IsDimmingEnabled;
		LoadAttributes[i].DimValue = temp[i].DimValue;
		LoadAttributes[i].DimMaxValue = temp[i].DimMaxValue;
		LoadAttributes[i].DimMinValue = temp[i].DimMinValue;
	}
	
}