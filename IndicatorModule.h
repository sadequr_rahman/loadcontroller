/*
 * IndicatorModule.h
 *
 * Created: 11/12/2015 10:53:32 AM
 *  Author: Aplomb_Tech
 */ 

#include "CommonHeader.h"


#ifndef INDICATORMODULE_H_
#define INDICATORMODULE_H_


#define LED11_12	PORTC0
#define LED9_10		PORTC1
#define LED7_8		PORTB1
#define LED5_6		PORTB2
#define LED3_4		PORTC2
#define LED1_2		PORTC3
#define LEDDDR0		DDRC
#define LEDDDR1		DDRB
#define LEDPORT0	PORTC
#define LEDPORT1    PORTB
#define LEDPIN0		PINC
#define LEDPIN1		PINB

#define LED_CTL		PORTB4
#define CTLPORT		PORTB
#define CTLDDR		DDRB


/*
IndicatorModuleInit(void);
			
This Function configure The I/O Pin for
controlling the Indicator LED. 
Must be called before using Indicator Module.
			 
*/
void IndicatorModuleInit(void);
/*
ColorMoodConfig(value)
			
This Function sets the color mood
of the module.

value -> 0 for single Color Mood
value -> 1 for Double Color Mood
*/
void IndicatorColorMoodConfig(uint8_t);
/*
IndicatorOnOff(LedID,value);
			
This Function is used to turn
on/off a specific led(LedID).

value -> 0 for Turn On
value -> 1 for Turn Off
*/
void IndicatorOnOff(uint8_t,uint8_t);
/*
getStatus(LedID);
			
This Function is used to get
Status of a specific led(LedID).
*/
uint8_t getStatus(uint8_t);

void IndicatorUpdateStatus(void);



#endif /* INDICATORMODULE_H_ */