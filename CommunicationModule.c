/*
 * CommunicationModule.c
 *
 * Created: 11/16/2015 12:33:34 PM
 *  Author: Aplomb_Tech
 */ 

#include "CommunicationModule.h"
#include "Debug.h"

volatile uint8_t _rx;
volatile uint8_t ByteCount = 0;
volatile char Byte[3];
volatile uint8_t SB_Rec_F = 0;

void ComModuleInit(unsigned int ubrr)
{
	/*Set baud rate */
	UBRR0H |= (unsigned char)(ubrr>>8);
	UBRR0L |= (unsigned char)ubrr;
	/*Enable receiver and transmitter and enable receive interrupt*/
	UCSR0B |= (1<<RXEN0)|(1<<TXEN0)|(1<<RXCIE0);
	/* Set frame format: 8data, 1stop bit */
	UCSR0C |= (3<<UCSZ00);
	/*Enable global interrupt*/
	sei();
}

void Transmit(uint8_t _data)
{
	/* Wait for empty transmit buffer */
	while ( !( UCSR0A & (1<<UDRE0)) );
	/* Put data into buffer, sends the data */
	UDR0 = _data;
}

void SendFeedBack()
{
	//uint8_t temp = 0;
	if (TxData.Flag == True){
		Transmit(FB_APP);
		}
	else{
		Transmit(FB_MAN);
		}
	
	 Transmit(TxData.Command);
	 Transmit(TxData.ChID);
	 Transmit(TxData._value);
}

/*
void feedBackUpdate(DataModel_t *Data)
{
	TxData.Command = Data->Command;
	TxData.ChID = Data->ChID;
	TxData._value = Data->_value;
	TxData.Flag = Data->Flag;
}
  */

ISR(USART_RX_vect)
{
	_rx = UDR0;
	if (SB_Rec_F == 1)
	{
			if (_rx == StatPacket)
			{
				ByteCount = 0;
				SB_Rec_F = 0;
			}
			
			else
			{
				Byte[ByteCount] = _rx;
				ByteCount++;
				if (ByteCount >= REC_BYTE_NUM)
				{
					SB_Rec_F =0;
					ByteCount = 0 ;
					RxData.Command	= Byte[0];
					RxData.ChID		= Byte[1];
					RxData._value	= Byte[2];
					RxData.Flag		= True;
					
				}
			}
			
		}
	if (_rx == StatPacket )
	{
		SB_Rec_F = 1;
	}

}