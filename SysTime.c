/*
 * SysTime.c
 *
 * Created: 11/13/2015 3:45:44 PM
 *  Author: Aplomb_Tech
 */ 


#include "SysTime.h"
volatile uint16_t count;

void Timer1Init()
{
	PRR &= ~(PRTIM1);
	OCR1AH = 0x1F;
	OCR1AL = 0x3F;
	TCCR1B |= (1<<WGM12);
	TCCR1B |= (1<<CS10);
	TIMSK1 |= (1<<OCIE1A);

}

uint16_t getmilis()
{
	return count;
	
}


ISR(TIMER1_COMPA_vect)
{
	count++;
}