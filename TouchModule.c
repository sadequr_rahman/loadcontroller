/*
 * i2c.c
 *
 * Created: 11/13/2015 12:44:44 PM
 *  Author: Aplomb_Tech
 */ 


#include "TouchModule.h"


void TouchModuleinit()
{
	TOUCH_RESET_DDR |= (1<<TOUCH_REST);
	TOUCH_RESET_PORT |= (1<<TOUCH_REST);
	PRR &= ~(1<<PRTWI);
	TWSR = 00;
	TWBR = 0x20;		//TWBR = 0x48 for 50KHz SCL ( TWBR = 0x20 for 100KHz SCL)
	TWCR = (1<<TWEN);
}

void TouchHardReset()
{
	   TOUCH_RESET_PORT &= ~(1<<TOUCH_REST);
	    _delay_ms(500);
	   TOUCH_RESET_PORT |= (1<<TOUCH_REST);
	    _delay_ms(500);

}

void twi_start()
{
	TWCR = (1<<TWSTA)|(1<<TWINT)|(1<<TWEN);
	 _delay_us(20);
	while(!(TWCR&(1<<TWINT)));
}

void twi_add(unsigned int _add)
{
	TWDR = _add;
	 _delay_us(20);
	TWCR = (1<<TWEN)|(1<<TWINT)|(1<<TWEA);
	 _delay_us(20);
	while(!(TWCR&(1<<TWINT)));
}

void twi_write(unsigned int _data)
{
	TWDR = _data;
	 _delay_us(20);
	TWCR = (1<<TWEN)|(1<<TWINT);
	 _delay_us(20);
	while(!(TWCR&(1<<TWINT)));
}

unsigned int twi_read()
{
	//TWCR &= ~(1<<TWEA);
	TWCR = (1<<TWINT)|(1<<TWEN);
	 _delay_us(20);
	while(!(TWCR&(1<<TWINT)));
	 _delay_us(20);
	return TWDR;
}

void twi_stop()
{
	TWCR = (1<<TWINT)|(1<<TWEN)|(1<<TWSTO);
	 _delay_us(20);
}
void writeReg(unsigned int _add,unsigned int _val)
{
	twi_start();
	twi_add(eKT5211_Write_Address);
	twi_write(_add);
	twi_write(_val);
	twi_stop();
}

unsigned int readReg(unsigned int _add)
{
	unsigned int _value;
	twi_start();
	twi_add(eKT5211_Write_Address);
	twi_write(_add);
	twi_start();
	twi_write(eKT5211_Read_Address);
	_value = twi_read();
	twi_stop();
	return _value;
}

unsigned int getButtonStatus1()
{
	unsigned int temp;
	temp = readReg(0x20);
	return temp;
}

void MultiButtonConfig(uint8_t _value)
{
	_value?writeReg(0x24,0):writeReg(0x24,0x40);
}
