/*
 * IndicatorModule.c
 *
 * Created: 11/12/2015 11:02:35 AM
 *  Author: Aplomb_Tech
 */ 


#include "IndicatorModule.h"
#include "LoadControlModule.h"

uint8_t allIndicatorOnF = 1;
static void ToggleLed(uint8_t);

volatile uint8_t timerTickCount = 0;
volatile uint8_t blink_f[6] = {0,0,0,0,0,0};
volatile uint8_t Blink_c[6]= {0,0,0,0,0,0};

void IndicatorModuleInit()
{
	LEDPORT0 &= ~((1<<LED1_2)|(1<<LED3_4)|(1<<LED9_10)|(1<<LED11_12));
	LEDPORT1 &= ~((1<<LED5_6)|(1<<LED7_8));
	CTLPORT &= ~((1 << LED_CTL));
	CTLDDR |= (1 << LED_CTL) ;
	CTLPORT |=	(1 << LED_CTL);
	LEDDDR0  |= (1<<LED1_2)|(1<<LED3_4)|(1<<LED9_10)|(1<<LED11_12);
	LEDDDR1  |= (1<<LED5_6)|(1<<LED7_8);
	allIndicatorOnF = 1;
	TIMSK2 |= (1<<OCIE2A);
	OCR2A	= 249;
	TCCR2A |= (1<<WGM21);
	TCCR2B |= (1<<CS21)|(1<<CS20);

}

void IndicatorColorMoodConfig(uint8_t _value)
{
	_value?setBit(CTLPORT,LED_CTL):clrBit(CTLPORT,LED_CTL);
}


void IndicatorOnOff(uint8_t ledID,uint8_t _value)
{
	switch (ledID)
	{
		case 0:
		_value?setBit(LEDPORT0,LED1_2):clrBit(LEDPORT0,LED1_2);
		_value?setBit(LEDPORT0,LED3_4):clrBit(LEDPORT0,LED3_4);;
		_value?setBit(LEDPORT1,LED5_6):clrBit(LEDPORT1,LED5_6);
		_value?setBit(LEDPORT1,LED7_8):clrBit(LEDPORT1,LED7_8);
		_value?setBit(LEDPORT0,LED9_10):clrBit(LEDPORT0,LED9_10);
		_value?setBit(LEDPORT0,LED11_12):clrBit(LEDPORT0,LED11_12);
		allIndicatorOnF = _value;
		_value?setBit(CTLPORT,LED_CTL):clrBit(CTLPORT,LED_CTL);
		break;
		case 1:
		(_value&&allIndicatorOnF)?setBit(LEDPORT0,LED1_2):clrBit(LEDPORT0,LED1_2);
		break;
		case 2:
		(_value&&allIndicatorOnF)?setBit(LEDPORT0,LED3_4):clrBit(LEDPORT0,LED3_4);
		break;
		case 3:
		(_value&&allIndicatorOnF)?setBit(LEDPORT1,LED5_6):clrBit(LEDPORT1,LED5_6);
		break;
		case 4:
		(_value&&allIndicatorOnF)?setBit(LEDPORT1,LED7_8):clrBit(LEDPORT1,LED7_8);
		break;
		case 5:
		(_value&&allIndicatorOnF)?setBit(LEDPORT0,LED9_10):clrBit(LEDPORT0,LED9_10);
		break;
		case 6:
		(_value&&allIndicatorOnF)?setBit(LEDPORT0,LED11_12):clrBit(LEDPORT0,LED11_12);
		break;
	}

	
}

 static void ToggleLed(uint8_t ledID)
 {
	 if (allIndicatorOnF == 1)
	 {
		 switch (ledID)
		 {
			 case 0:
			 toggleBit(LEDPORT0,LED1_2);
			 break;
			 case 1:
			 toggleBit(LEDPORT0,LED3_4);
			 break;
			 case 2:
			 toggleBit(LEDPORT1,LED5_6);
			 break;
			 case 3:
			 toggleBit(LEDPORT1,LED7_8);
			 break;
			 case 4:
			 toggleBit(LEDPORT0,LED9_10);
			 break;
			 case 5:
			 toggleBit(LEDPORT0,LED11_12);
			 break;
		 }
	 }
 }

void IndicatorUpdateStatus(void)
{
	for (uint8_t i = 0; i<6; i++)
	{
		LoadAttributes[i].LoadStatus?IndicatorOnOff(i+1,True):IndicatorOnOff(i+1,False);
	}
}


ISR(TIMER2_COMPA_vect)
{
	timerTickCount++;
	if(timerTickCount >= 200)
	{
		for (uint8_t i = 0; i < 6 ; i ++)
		{
			if (blink_f[i] == True)
			{
				ToggleLed(i);
				Blink_c[i]++;
				if (Blink_c[i] >= 2)
				{
					blink_f[i] = False;
					Blink_c[i] = 0;
				}
			}
		}
		timerTickCount = 0;
	}
}