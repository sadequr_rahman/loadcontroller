/*
 * Debug.h
 *
 * Created: 11/13/2015 3:05:41 PM
 *  Author: Aplomb_Tech
 */ 


#ifndef DEBUG_H_
#define DEBUG_H_

#include "CommonHeader.h"
#include <stdlib.h>

void DebugInit(unsigned int);
void DebugSTR(const char*);
void DebugCHAR(char);
void DebugUINT(uint16_t);


#endif /* DEBUG_H_ */